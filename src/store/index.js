import { createStore } from "vuex";

export default createStore({
  state: {
    user: {
      token: '',
      username: '',
      name: '',
      role: ''
    },
    ticket: {
      t_id: '',
      t_name: '',
      t_email: '',
      t_type: '',
      t_msg: ''
    },
    navList_items: {
      admin: [
        {id: 0, name:'MyProfile', item: 'My Profile'},
        {id: 1, name:'TicketInfo', item: 'All Ticket Info'},
        {id: 2, name:'TicketForm', item: 'Add New Ticket'},
        {id: 3, name:'UserInfo', item: 'All User Info'},
        {id: 4, name:'RegisterForm', item: 'Add New User'},
        {id: 5, name:'Logout', item: 'Logout'}
      ],
      support_person: [
        {id: 0, name:'MyProfile', item: 'My Profile'},
        {id: 1, name:'TicketInfo', item: 'All Ticket Info'},
        {id: 2, name:'Logout', item: 'Logout'}
      ],
      customer: [
        {id: 0, name:'MyProfile', item: 'My Profile'},
        {id: 1, name:'TicketInfo', item: 'My Tickets'},
        {id: 2, name:'TicketForm', item: 'Add New Ticket'},
        {id: 3, name:'Logout', item: 'Logout'}
      ]
    }
  },
  mutations: {},
  actions: {},
  modules: {},
});
