import axios from "axios";

const apiClient = axios.create({
  baseURL: "http://127.0.0.1:5000",
  withCredentials: false,
  headers: {
    Accept: "application/json",
    "Content-Type": "application/json"
  }
})

export default {
  postUser(userInfo) {
    return apiClient.post('/register', userInfo)
  },
  postLogin(uname,pass) {
    return apiClient.post('/login', {}, {auth:{username:uname,password:pass}})
  },
  getTicketInfo(utoken) {
    return apiClient.post('/ticket_info', {}, { headers: {'token': utoken}})
  },
  getUserInfo(utoken) {
    return apiClient.post('/user_info', {}, { headers: {'token': utoken}})
  },
  postUserRole(utoken, user_id, userRole) {
    return apiClient.post('/edit_user_role/'+user_id, userRole, { headers: {'token': utoken}})
  },
  postTicket(utoken, ticketInfo) {
    return apiClient.post('/add_ticket', ticketInfo, { headers: {'token': utoken}})
  },
  postReply(utoken, tid, replyInfo) {
    return apiClient.post('/reply/'+tid, replyInfo, { headers: {'token': utoken}})
  },
  getAllReply(utoken, tid) {
    return apiClient.post('/reply_info/'+tid, {}, { headers: {'token': utoken}})
  }
}
